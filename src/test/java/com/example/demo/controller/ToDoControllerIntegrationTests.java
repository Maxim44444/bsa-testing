package com.example.demo.controller;

import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles(profiles = "test")
class ToDoControllerIntegrationTests {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    ToDoRepository repository;

    private static final List<ToDoEntity> testTodos;

    static {
        testTodos = Arrays.asList(
                new ToDoEntity("Wash the dishes"),
                new ToDoEntity("Learn to test Java app").completeNow(),
                new ToDoEntity("Complete the task"),
                new ToDoEntity("Write integration tests").completeNow()
        );
    }

    @BeforeEach
    void setUp() {
        repository.saveAll(testTodos);
    }

    @AfterEach
    void tearDown() {
        repository.deleteAll();
    }

    @Test
    void whenGetTodos_thenReturnAllTodos() throws Exception {
        this.mockMvc
                .perform(get("/todos"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(4)));
    }

    @Test
    void whenGetTodoByIdWhichIsNotExists_thenThrowToDoNotFoundException() throws Exception {
        this.mockMvc
                .perform(get("/todos/" + -1L))
                .andExpect(status().isOk())
                .andExpect(mvcResult -> assertTrue(mvcResult.getResolvedException() instanceof ToDoNotFoundException));
    }

    @Test
    void whenSaveTodo_thenReturnSavedTodoWithId() throws Exception {
        var todo = new ToDoEntity("Write integration tests");

        this.mockMvc
                .perform(post("/todos")
                .contentType(MediaType.APPLICATION_JSON)
                .content(String.format("{ \"text\": \"%s\" }", todo.getText())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").value(todo.getText()))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenGetCompleted_thenReturnResponseWithTodosWhereCompletedAtIsNotNull() throws Exception {
        var expectedResult = testTodos.stream()
                .filter(todo -> todo.getCompletedAt() != null)
                .collect(Collectors.toList());

        this.mockMvc
                .perform(get("/todos/completed"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(expectedResult.get(0).getText()))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").exists())
                .andExpect(jsonPath("$[1].text").value(expectedResult.get(1).getText()))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].completedAt").exists());
    }

    @Test
    void whenGetTodosInProgress_thenReturnResponseWithTodosWhereCompletedAtIsNull() throws Exception {
        var expectedResult = testTodos.stream()
                .filter(todo -> todo.getCompletedAt() == null)
                .collect(Collectors.toList());

        this.mockMvc
                .perform(get("/todos/inProgress"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].text").value(expectedResult.get(0).getText()))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].completedAt").doesNotExist())
                .andExpect(jsonPath("$[1].text").value(expectedResult.get(1).getText()))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].completedAt").doesNotExist());
    }

    @Test
    void whenCompleteById_thenSetCompletedTimeAndReturnTodo() throws Exception {
        var todo = repository.findAll().stream()
                .filter(t -> t.getCompletedAt() == null)
                .findAny().orElseThrow();

        this.mockMvc
                .perform(put("/todos/" + todo.getId() + "/complete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").value(todo.getText()))
                .andExpect(jsonPath("$.completedAt").exists());
    }

    @Test
    void whenCancelComplete_thenSetCompletedNullAndReturnTodo() throws Exception {
        var todo = repository.findAll().stream()
                .filter(t -> t.getCompletedAt() != null)
                .findAny().orElseThrow();

        this.mockMvc
                .perform(put("/todos/" + todo.getId() + "/cancelCompleted"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").exists())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.text").value(todo.getText()))
                .andExpect(jsonPath("$.completedAt").doesNotExist());
    }

    @Test
    void whenDeleteById_thenFindAndDeleteTodoByItsId() throws Exception {
        var todo = repository.findAll().stream()
                .findAny().orElseThrow();

        this.mockMvc
                .perform(delete("/todos/" + todo.getId()))
                .andExpect(status().isOk());

        assertTrue(repository.findById(todo.getId()).isEmpty());
    }

    @Test
    void whenClearList_thenDeleteAllTodos() throws Exception {
        this.mockMvc
                .perform(delete("/todos/clearList"))
                .andExpect(status().isOk());

        assertTrue(repository.findAll().isEmpty());
    }

}
