package com.example.demo.controller;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import javax.validation.Valid;

import com.example.demo.dto.ToDoResponse;
import com.example.demo.dto.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.service.ToDoService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class ToDoController {

	@Autowired
	ToDoService toDoService;
	
	@ExceptionHandler({ ToDoNotFoundException.class })
	public String handleException(Exception ex) {
		return ex.getMessage();
	}
	
	@GetMapping("/todos")
	@Valid
	public List<ToDoResponse> getAll() {
		return toDoService.getAll();
	}

	@GetMapping("/todos/completed")
	public List<ToDoResponse> getCompleted() {
		return toDoService.getCompleted();
	}

	@GetMapping("/todos/inProgress")
	public List<ToDoResponse> getTodosInProgress() {
		return toDoService.getTodosInProgress();
	}

	@PostMapping("/todos")
	@Valid
	public ToDoResponse complete(@Valid @RequestBody ToDoSaveRequest todoSaveRequest) throws ToDoNotFoundException {
		return toDoService.upsert(todoSaveRequest);
	}

	@PutMapping("/todos/{id}/complete")
	@Valid
	public ToDoResponse complete(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.completeToDo(id);
	}

	@PutMapping("/todos/{id}/cancelCompleted")
	@Valid
	public ToDoResponse cancelCompleted(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.cancelCompleteToDo(id);
	}

	@GetMapping("/todos/{id}")
	@Valid
	public ToDoResponse getOne(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.getOne(id);
	}

	@DeleteMapping("/todos/{id}")
	public void delete(@PathVariable Long id) {
		toDoService.deleteOne(id);
	}

	@DeleteMapping("/todos/clearList")
	public void clearList() {
		toDoService.deleteAll();
	}

}